#!/system/bin/sh

#-- 1 -- Set the root file system to writable
mount -o remount,rw / 

#-- 2 -- Set up the network 
ip addr add 192.168.1.125/24 dev eth0 label eth0
ip link set eth0 up
ip route add default via 192.168.1.1 dev eth0

#-- 3 --Start all init scripts in /system/etc/init.d  executing them in numerical order.
for i in /system/etc/init.d/S??* ;do

     # Ignore dangling symlinks (if any).
     [ ! -f "$i" ] && continue
     echo  $i
     case "$i" in
	*.sh)
	    # Source shell script for speed.
	    (
		trap - INT QUIT TSTP
		set start
		. $i
	    )
	    ;;
	*)
	    # No sh extension, so fork subprocess.
	    $i start
	    ;;
    esac
done

#-- 4 --
